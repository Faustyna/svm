===
svm
===

Please read this carefully...


Description
===========

Fast initialization:
1. Create a new directory.
2. git init
3. git clone https://Faustyna@bitbucket.org/Faustyna/svm.git


How to use
==========

docs-> how_to_use.rst
Consider, that sometimes it takes some time to compute a result.

Docs
====

You can easily create a svm module documentation using pydoc. 
Example configuration:
<path>\pydoc.py -w svm 


Note
====

Some useful apps, materials: <add your own>

WinMerge - simply app, which shows difference between two files ( just drag and drop two files and look what has been done).
SourceTree - provides a graphical interface for our repository. Easy and beautiful. Unfortunately there is no stable version...
http://www.lfd.uci.edu/~gohlke/pythonlibs/ - Python libraries for Windows. All of them you can install using pip. I've tested it. It works :)

This project has been set up using PyScaffold 2.4.4. For details and usage information on PyScaffold see http://pyscaffold.readthedocs.org/.
