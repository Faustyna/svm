import unittest
import numpy as np

from ourSVM import SVMalgorithm
from kernel import Kernel

class SVMTrainerTests(unittest.TestCase):

    def test_getPredictorTest_1(self):
        """ Nothing happens here.
        """
        pass

    def test_getPredictorTest_2(self):
        """ Tested data set is like in an svm_example.
        """
        num_samples= 10;
        num_features= 2;
        samples = np.matrix(np.random.normal(size=num_samples * num_features)
                        .reshape(num_samples, num_features))
        labels = 2 * (samples.sum(axis=1) > 0) - 1.0
        svmTrainer = SVMalgorithm(Kernel.linear(), 0.1)
        lagrangeMultipliers=svmTrainer.computeLagrangeMultipliers(samples, labels)
        svmTrainer.computeLagrangeMultipliers(samples, labels)
		#self.assertTrue()
        pass

    def test_getPredictorTest_3(self):
        """ Tested data set is like in the 1st example from SVM.pdf
        """
        samples = np.matrix('3,-2; 4,-3; 3,-5; -1,2; -3,3; -4,3')
        labels = np.matrix('1.; 1.; 1.; -1.; -1.; -1.')
        svmTrainer = SVMalgorithm(Kernel.linear(), 0.1)
        lagrangeMultipliers=svmTrainer.computeLagrangeMultipliers(samples, labels)
        svmTrainer.computeLagrangeMultipliers(samples, labels)
        #self.assertTrue()
        pass

    def test_getPredictorTest_4(self):
        """Tested data set is like in the 3rd example from SVM.pdf
        """
        samples = np.matrix('1, 1; 3,-2; 3,-4; 4,-3; -1,2; -1,-1; -3,1; -3,3; -4,3')
        labels = np.matrix('1.; 1.; 1.; 1.; -1.; -1.; -1.; -1.; -1.')
        svmTrainer = SVMalgorithm(Kernel.linear(), 0.1)
        lagrangeMultipliers=svmTrainer.computeLagrangeMultipliers(samples, labels)
        svmTrainer.computeLagrangeMultipliers(samples, labels)
        #self.assertTrue()
        pass
		
		

def main():
    unittest.main()

if __name__ == '__main__':
    main()