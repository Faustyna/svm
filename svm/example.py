#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import numpy as np
import matplotlib.pyplot as plot
import matplotlib.cm as cm
import itertools
import argh

from ourSVM import SVMalgorithm
from kernel import Kernel

__author__ = "MasterTeam"
__copyright__ = "YES"
__license__ = "none"

_logger = logging.getLogger(__name__)

def createPlot(samples, labels, hyperplaneFactors, gridSize, fileName):
    """Function to draw plot with results of SVM algorithm in some destination file.
    Parameters
    ----------
    svmPredictor :     SVMPredictor class object
    X            :     Array of samples
    y            :     Array of support vector labels
    gridSize     :     Grid size
    fileName     :     Name of destination file
    """
   
    x_min, x_max = samples[:, 0].min() - 1, samples[:, 0].max() + 1
    y_min, y_max = samples[:, 1].min() - 1, samples[:, 1].max() + 1

    points=np.transpose(samples)
    points_x=points[0,:]
    points_y=points[1,:]
    
    labels_indicate = labels > 0
    labels_indicate=np.transpose(labels_indicate)
	
    points_x_pos=points_x[labels_indicate]
    points_y_pos=points_y[labels_indicate]

    points_x_neg=points_x[~labels_indicate]
    points_y_neg=points_y[~labels_indicate]
	
    hyperplane=(-1*hyperplaneFactors[0]/hyperplaneFactors[1], -1*hyperplaneFactors[2]/hyperplaneFactors[1])
    hyperplane_x=(x_min, x_max)
    hyperplane_y=(hyperplane[0]*x_min+hyperplane[1], hyperplane[0]*x_max+hyperplane[1])
	
    plot.plot(points_x_pos, points_y_pos, marker='o', color='r')
    plot.plot(points_x_neg, points_y_neg, marker='o', color='b')
    plot.plot(hyperplane_x,hyperplane_y)

    plot.xlim(x_min, x_max)
    plot.ylim(y_min, y_max)
    plot.show()
    plot.savefig(fileName)

def main(numSamples=500, numFeatures=2, gridSize=20, fileName="results.pdf"):
    """Main function to train SVM algorithm. 

    Parameters
    ----------
    numSamples   :   Number of samples 
    numFeatures  :   Number of features
    gridSize     :   Grid size
    fileName     :   Name of destination file
    """
    #samples = np.matrix('3,-2; 4,-3; 3,-5; -1,2; -3,3; -4,3')
    #labels = np.matrix('1.; 1.; 1.; -1.; -1.; -1.')
    #samples = np.matrix('1, 1; 3,-2; 3,-4; 4,-3; -1,2; -1,-1; -3,1; -3,3; -4,3')
    #labels = np.matrix('1.; 1.; 1.; 1.; -1.; -1.; -1.; -1.; -1.')
    samples = np.matrix(np.random.normal(size=numSamples * numFeatures)
                        .reshape(numSamples, numFeatures))
    labels = 2 * (samples.sum(axis=1) > 0) - 1.0
    svmTrainer = SVMalgorithm(Kernel.linear(), 0.1)
    lagrangeMultipliers=svmTrainer.computeLagrangeMultipliers(samples, labels)
    hyperplaneFactors=svmTrainer.getHyperplaneFactors(samples, labels, lagrangeMultipliers)

    createPlot(samples, labels, hyperplaneFactors, gridSize, fileName)

if __name__ == "__main__":
    logging.basicConfig(level=logging.ERROR)
    argh.dispatch_command(main)