import numpy as np
import cvxopt.solvers
import logging

MIN_SUPPORT_VECTOR_MULTIPLIER = 1e-5

class SVMalgorithm(object):
    """Class shows an implementation of SVM algorithm. 
    It finds a optimum separation hyperplane(a SVM Predictor), 
    which separates a data with the maximum margin for a given finite set of learning patterns.
    It uses a solvers.qp  from cvxopt library to find Lagrange multipliers.
    
    To use it:
    ----------
    1. Create a SVMTrainer object.
    2. Compute the Lagrange Multipliers by computeLagrangeMultipliers.
    3. Compute HyperPlaneFactors by getHyperplaneFactors.
    
    Note:
    -----
    You can plot a SVM hyperplane using matplotlib.
    """

    def __init__(self, kernel, accuracy):
        """Function creates a new SVMPredictor.
        
        Parameters
        ----------
        kernel   :    Kernel class object.
        accuracy :    Calculations accuracy.
        """
        self._kernel = kernel
        self._accuracy = accuracy
        self._hyperplane_factors = [0,0,0]
        self._lagrange_multipliers = [0]
        self._predictor = [0]

    def _getKernelMatrix(self, mTrainingMatrix): 
        """Function returns a new kernel transformation matrix computes from mTrainingMatrix.
        """
	
        nbr_of_samples, samples_dimensions = mTrainingMatrix.shape
        kernelMatrix = np.zeros((nbr_of_samples, nbr_of_samples))
		
        for i, x_i in enumerate(mTrainingMatrix):
            for j, x_j in enumerate(mTrainingMatrix):
                kernelMatrix[i, j] = self._kernel(x_i, x_j)
        return kernelMatrix

    def computeLagrangeMultipliers(self, mTrainingMatrix, mLabels):
        """Function computes Lagrange multipliers. It uses a solvers.qp  from cvxopt to resolve a quadratic equal.
        """
        nbr_of_samples, samples_dimensions = mTrainingMatrix.shape

        kernelMatrix = self._getKernelMatrix(mTrainingMatrix)                           # Getting a kernel Matrix.

        P = cvxopt.matrix(np.outer(mLabels, mLabels) * kernelMatrix)                    # mLabels*mLabels^T * kernelMatrix 
        q = cvxopt.matrix(-1 * np.ones(nbr_of_samples))

        #Here we can modify  G and h matrix to get a soft-margin, but how?
        negativeDiagonalMatrix = cvxopt.matrix(np.diag(np.ones(nbr_of_samples) * -1))	
        zerosMatrix = cvxopt.matrix(np.zeros(nbr_of_samples))
        positiveDiagonalMatrix = cvxopt.matrix(np.diag(np.ones(nbr_of_samples)))
        onesMatrix = cvxopt.matrix(np.ones(nbr_of_samples) * self._accuracy)

        G = cvxopt.matrix(np.vstack((negativeDiagonalMatrix, positiveDiagonalMatrix)))  # Concatenation of 2 diagonal Matrix: positive and negative.
        h = cvxopt.matrix(np.vstack((zerosMatrix, onesMatrix)))                         # Concatenation of 2 Matrix: one filled with zeros, second filled with ones.

        A = cvxopt.matrix(mLabels, (1, nbr_of_samples))                                 # Creates a matrix from mLabels with dims: [1, nbr_samples] (transposition)
        b = cvxopt.matrix(0.0)                                                          # Matrix with one element: 0

        solution = cvxopt.solvers.qp(P, q, G, h, A, b)                                  # The resolution of a quadratic equal. -> We've got Lagrange multipliers. Done!

        self._lagrange_multipliers = np.ravel(solution['x'])                                                  # Returns Lagrange multipliers as array.
        return self._lagrange_multipliers

    def getHyperplaneFactors(self, mTrainingMatrix, mLabels, lagrange_multipliers):
        """Given the training features mTrainingMatrix with labels mLabels, returns a SVM
        predictor representing the trained SVM.
        """
        omega1=[0]
        omega2=[0]
        b=[0]
        support_vector_indices = lagrange_multipliers > MIN_SUPPORT_VECTOR_MULTIPLIER

        support_multipliers = lagrange_multipliers[support_vector_indices]      # Choosing support multipliers, which are bigger than MIN_SUPPORT_VECTOR_MULTIPLIER
        support_vectors = mTrainingMatrix[support_vector_indices]               # Choosing support vectors.
        support_vector_labels = mLabels[support_vector_indices]                 # Choosing labels of support vectors.

        for i in range(len(support_multipliers)):
            omega1+= (support_multipliers[i]*support_vectors[i,0]*support_vector_labels[i])
            omega2+= (support_multipliers[i]*support_vectors[i,1]*support_vector_labels[i])
 
        b= (((-1* omega1*support_vectors[0,0] -omega2*support_vectors[0,1]) +(-1* omega1*support_vectors[1,0] -omega2*support_vectors[1,1]))/2)
        self._hyperplane_factors[0]=omega1[0,0]
        self._hyperplane_factors[1]=omega2[0,0]
        self._hyperplane_factors[2]=b[0,0]
        return self._hyperplane_factors
		

    def getPredictor(self, sample, hyperplane_factors):
        """Given the training features mTrainingMatrix with labels mLabels, returns a SVM
        predictor representing the trained SVM.
        """
        if((sample[0,0]*hyperplane_factors[0]+sample[0,1]*hyperplane_factors[1] +hyperplane_factors[2])>1):
            self._predictor = 1
        if((sample[0,0]*hyperplane_factors[0]+sample[0,1]*hyperplane_factors[1] +hyperplane_factors[2])<-1):
            self._predictor = -1
        else:
            self._predictor = 0
        return self._predictor
        
